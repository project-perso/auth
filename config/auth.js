// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {
    
    'facebookAuth' : {
        'clientID'        : '1723903917863092', // your App ID
        'clientSecret'    : 'bef29a000e3a8abb90468d9ebeaa5e7c', // your App Secret
        'callbackURL'     : 'http://localhost:8080/auth/facebook/callback'
    },

    'twitterAuth' : {
        'consumerKey'        : 'your-consumer-key-here',
        'consumerSecret'     : 'your-client-secret-here',
        'callbackURL'        : 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth' : {
        'clientID'         : '899007865081-hqtd8tnk0pbt4omlgs8rtpckms96aq5r.apps.googleusercontent.com',
        'clientSecret'     : 'oY8nlSUfr6ENQcFUaNdE9F_9',
        'callbackURL'      : 'http://localhost:8080/auth/google/callback'
    }

};
