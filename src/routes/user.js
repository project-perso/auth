app.get('/', function(req, res) {
    res.render('index.ejs');
});

app.get('/profile', isLoggedIn, function(req, res) {
    res.render('profile.ejs', {
        user : req.user
    });
});

app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

app.get('/login', function(req, res) {
    res.render('login.ejs', { message: req.flash('loginMessage') });
});
