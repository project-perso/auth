var route = require('express').Router(),
    config = require('../../config.json'),
    User = require('../models/user');

route.get('/', function(req, res) {
    if(req.user){
	res.render('profile.ejs', {
	    user : req.user,
	    app: config.application.name
	});
    }else{
        res.render('login.ejs', { message: req.flash('message'), app: config.application.name });
    }
});

route.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

route.get('/delete', function(req, res) {
    if(req.user){
        return User.delete(req.user.id)
            .then(ok => {
                req.logout();
                res.redirect('/')
            })
            .catch(err => {
                req.flash('message', 'an error occur, try again in the next few minutes')
                res.redirect('/')
            });
    }else{
        res.redirect('/')
    }
});

module.exports = route;
