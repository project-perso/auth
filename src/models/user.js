var db = require('../lib/db'),
    q = require('q'),
    uuid = require('uuid');


var find = {
    byId: function(user_id){
        var query = `
        SELECT * FROM auth_user_account ua JOIN auth_user as u ON u.id = ua.related_user
        WHERE ua.related_user = $1
        `
        return db.query(query, [user_id])
	    .then(res => {
                if(res.rows.length === 0){
                    return q.reject('We don\'t know you.')
                }
                return this._hydrateUser(res.rows);
	    });
    },
    byEmails: function(email){
        var query = `
        SELECT * FROM auth_user_account ua JOIN auth_user as u ON u.id = ua.related_user
        WHERE
        (ua.profile ? 'email' AND ua.profile->>'email' = $1) OR
        (ua.profile ? 'emails' AND ua.profile->'emails' ?| array[$1]);
        `;
        return db.query(query, [
            email
        ]).then(res => {
            if(res.rows.length === 0){
                return q.reject('We don\'t know you.')
            }
            return this._hydrateUser(res.rows)
	});
    },
    _hydrateUser: function(rows){
        var user = rows.reduce((acc, el) => {
            if(!acc.id){
                acc.id = el.id,
                acc.status = el.status;
            }
            acc[el.provider] = el.profile;
            return acc;
        }, {});
        return user.id? q.when(user) : q.reject('oups');
    }
};
var verify = {
    send: function(email){
	return find.byEmails(email)
	    .then(user => this._createLink)
	    .then(link => this._send);
    },
    _createLink: function(user){
    },
    _send: function(link){
    }
}

module.exports.find = find;

var update = {
    byId: function(user_id, user){

    }
}
module.exports.update = function(user){
    if(user.id){
	var query = "UPDATE auth_user_account SET profile = $1 WHERE related_user = $2 AND provider = $3";
	return db.query(query, [user.profile, user.id, user.provider]);
    }else{
	return q.reject('user id is not valid!');
    }
}

module.exports.create = function(user, provider){
    user.id = uuid.v1();
    return db.query('BEGIN')
        .then(e=> db.query('INSERT INTO auth_user(id, status) VALUES($1, $2)', [user.id, user.status]))
        .then(e=> db.query('INSERT INTO auth_user_account(related_user, provider, profile) VALUES($1, $2, $3)', [user.id, user.provider, user.profile]))
        .then(e=> db.query('COMMIT'))
        .then(e=> find.byId(user.id))
        .catch(e=> {
            db.query('ROLLBACK')
            return q.reject('internal error');
        });
}

module.exports.delete = function(user_id){
    return user_id ? db.query("DELETE FROM auth_user WHERE id = $1", [user_id]) : q.reject('no user id');
}
