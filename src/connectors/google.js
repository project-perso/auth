var User = require('../models/user'),
    GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
    helper = require('../lib/helper');

module.exports.init = function(passport){
    var cfg = helper.getConfig('google');
    passport.use(new GoogleStrategy({
	clientID        : cfg.client_id,
	clientSecret    : cfg.client_secret,
	callbackURL     : cfg.redirect_uri,
	passReqToCallback : true
    }, _upsert));
}

module.exports.route = function(router, passport){
    router.get('/auth', passport.authenticate('google', {scope: ['profile', 'email']}));
    router.get('/auth/callback', passport.authenticate('google', {
    	successRedirect: '/',
    	failureRedirect: '/'	
    }));
    return router;
}




function _upsert(req, access_token, refreshToken, profile, cb) {
    var userObj = {
	id: profile.id,
	provider: 'google',
	data: {
	    profile: profile,
	    token: {
		access_token: access_token,
		refresh_token: refresh_token
	    }
	}
    };
    if(req.user){
	User.update
    }else{	
	User.create
    }
};
