var User = require('../models/user'),
    helper = require('../lib/helper'),
    bcrypt = require('bcrypt'),
    q = require('q'),
    ejs = require('ejs'),
    LocalStrategy = require('passport-local').Strategy;

module.exports.init = function(passport){
    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, _login));

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, _register));
}


module.exports.route = function(router, passport, isAuthenticated){
   
    router.get('/password', function(req, res){
	if(req.user){
	    res.redirect('/');
	}else{
	    res.render('password_lost', {message: null, withForm: true});
	}
    });

    router.get('/password/:code', function(req, res){
	if(req.params.code){
	    helper.crypto.decode(req.params.code)
		.then(_validatePasswordReset)
		.then(data => res.render('password_recover', {code: req.params.code, message: null}))
		.catch(err => res.render('password_lost', {message: 'this link isn\'t valid, please try again', withForm: true}));
	}else{
	    res.render('password_lost', {message: null, withForm: true});
	}
    });

    router.post('/login', passport.authenticate('local-login', {
        successRedirect : '/',
        failureRedirect : '/?login',
        failureFlash : true
    }));

    router.post('/register', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/?register',
        failureFlash : true
    }));

    
    router.post('/password', function(req, res){
	// STEP 1 of password recover process: enter your email
	if(req.body.email){
	    var data = {
		validUntil: new Date().getTime() + 1000*60*60*2, // 2 hours
		email: req.body.email.toLowerCase().trim()
	    };
	    return User.find.byEmails(req.body.email)
		.then(user => {
		    return helper.crypto.encode(data)
			.then(str => helper.render.asHTML('emails/password_reset', {code:str, base_url: 'test.com'}))
			.then(str => helper.mailer.send(str, data.email))
			.then(e => {
			    res.render('password_lost', {
				message: 'You should have received an email. Follow the instructions it contains and you should be all set!',
				withForm: false
			    });
			})
			.catch(e => {
			    res.render('password_lost', {
				message: 'Oups we ran into an error',
				withForm: true
			    });  
			});
		})
		.catch(err => {
		    res.render('password_lost', {
			message: 'You doesn\'t seem to have an account',
			withForm: true
		    }); 
		})
	}
	// STEP 2: user came in using a link
	else if(req.body.password && req.body.password_check){
	    if(req.body.password !== req.body.password_check){
		res.render('password_recover', {
		    message: 'The passwords are different',
		    code: req.body.code
		});
	    }else{
		helper.crypto.decode(req.body.code || '')
		    .then(obj => _validatePasswordReset(obj))
		    .then(email => User.find.byEmails(email))
		    .then(user => {
			var userObj = {
			    id: user.id,
			    provider: 'local',
			    profile: {
				email: user.local.email,
				password: bcrypt.hashSync(req.body.password, 5)
			    }
			};
			return User.update(userObj)
		    })
		    .then(ok => res.redirect('/'))
		    .catch(err => {
			console.log(err)
			res.render('password_recover', {
			    message: 'something unexpected happen. Please try again',
			    code: req.body.code
			})
		    });
	    }
	}else{
	    res.render('password_lost', {
		message: 'You need to fill the form',
		withForm: true
	    });
	};
    });

    return router;
}



//////////////////////////////////////////////////////////////////
// HANDLERS

function _login(req, email, password, cb){
    if (email) email = email.toLowerCase();
    if(req.user) return cb(null, req.user);
    User.find.byEmails(email)
	.then(user => {
	    if(user.local){
		verifyPassword(user.local.password, password)
		    .then(ok => cb(null, user))
		    .catch(err=> cb(null, null, req.flash('message', 'Wrong password')));
	    }else{
		var keys = Object.key(user).filter(e => e === 'local' ? false: true);
		cb(null, null, req.flash('message', 'Your account is link to '+keys.join(', ')))
	    }
	})
	.catch(err => {
	    cb(null, null,  req.flash('message', 'There is no user with this email address'))
	});	    

    function verifyPassword(password_hash, password){
	var def = q.defer();
	bcrypt.compare(password, password_hash, function(err, match){
	    if(err || match !== true) def.reject('nop')
	    else def.resolve('yep')
	});
	return def.promise;
    }
}

function _register(req, email, password, cb) {
    if(!email || !password ) cb(null, null, req.flash('message', 'you need an email and a password to connect!'));
    var userObj = {
        status: 'active',
        provider: 'local',
        profile: {
            email: email.toString().toLowerCase().trim(),
            password: bcrypt.hashSync(password, 5)
        }
    };
    User.find.byEmails(email)
        .then(user => {
            if(user.local){
                cb(null, null, req.flash('message', 'you already have an account!'))
            }else{
                cb(null, null, req.flash('message', 'TODO'))
                // we assume we can merge both accounts
                // User.update(userObj, user.id)
                //     .then(usr => cb(null, usr))
                //     .catch(err => cb(null, null, req.flash('message', 'internal error')));
            }
        })
        .catch(nop => {
            User.create(userObj)
                .then(usr => {
                    cb(null, usr)
                })
                .catch(err => {
                    cb(null, null, req.flash('message', 'internal error'))
                });
        });
}

//////////////////////////////////////////////////////////////////////
// HELPERS
function _validatePasswordReset(obj){
    if(obj.validUntil - new Date().getTime() > 0 && obj.email){
	return q.when(obj.email)
    }else{
	return q.reject('not valid');
    }
}
