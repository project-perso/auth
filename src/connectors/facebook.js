var User = require('../models/user'),
    FacebookStrategy = require('passport-facebook').Strategy,
    helper = require('../lib/helper');

module.exports.init = function(passport){
    var cfg = helper.getConfig('facebook');
    passport.use(new FacebookStrategy({
	clientID        : cfg.client_id,
	clientSecret    : cfg.client_secret,
	callbackURL     : cfg.redirect_uri,
	passReqToCallback : true
    }, _upsert));
}

module.exports.route = function(router, passport){
    router.get('/auth', passport.authenticate('facebook', {scope: 'email'}));
    router.get('/auth/callback', passport.authenticate('facebook', {
    	successRedirect: '/',
    	failureRedirect: '/'	
    }));
    return router;
}




function _upsert(req, access_token, refreshToken, profile, cb) {
    var userObj = {
	id: profile.id,
	provider: 'facebook',
	data: {
	    profile: profile,
	    token: {
		access_token: access_token,
		refresh_token: refresh_token
	    }
	}
    };
    console.log(userObj)
    if(req.user){
	User.update
    }else{	
	User.create
    }
};
