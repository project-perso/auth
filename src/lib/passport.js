var helper           = require('./passport_helpers'),
    LocalStrategy    = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy  = require('passport-twitter').Strategy,
    GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;


var User       = require('../models/user');
var configAuth = require('./../../config/auth'); // use this one for testing

module.exports.init = function(passport){
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function(id, cb) {
	User.find(id)
	    .then(user => cb(null, user))
	    .catch(err => cb(err));
    });
}
module.exports.local = function(passport){    
    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, helper.local.login));

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, helper.local.register));
}

module.exports.facebook = function(passport){
    passport.use(new FacebookStrategy({
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        profileFields   : ['id', 'name', 'email'],
        passReqToCallback : true
    }, helper.facebook));
}

module.exports.google = function(passport){
    passport.use(new GoogleStrategy({
	clientID        : configAuth.googleAuth.clientID,
	clientSecret    : configAuth.googleAuth.clientSecret,
	callbackURL     : configAuth.googleAuth.callbackURL,
	passReqToCallback : true
    }, helper.google));
}
