var config = require('../../config.json'),
    q = require('q'),
    ejs = require('ejs'),
    crypto = require('crypto'),
    nodemailer = require('nodemailer'),
    User = require('../models/user'),
    middleware = {
        isAuthenticated: function(req, res, next){
	        if (req.isAuthenticated())
                return next();
	        res.redirect('/');
        }
    },
    email = nodemailer.createTransport(config.mail.connection_string);


module.exports.config = config;
module.exports.getConfig = function(key){
    var cfg = config.connectors.filter(data => data.provider === key? true: false)
    return cfg.length === 1? cfg[0] : null;
}

module.exports.init = {
    setup: function(app){
	    var mongoose = require('mongoose'),
	        flash    = require('connect-flash'),
	        morgan       = require('morgan'),
	        cookieParser = require('cookie-parser'),
	        bodyParser   = require('body-parser'),
	        session      = require('express-session'),
		express      = require('express');

        // a bit of express setup
        app.use(morgan('dev'));
	app.use(cookieParser());
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	app.set('view engine', 'ejs');
	app.use(session({
	    secret: config.server.secret_key,
	    // cookie: {
	    // 	domain: 'youplo.com'
	    // }
	}));
	app.use(flash());
    },
    authentication: function(app){
	var passport = require('passport'),
	    express  = require('express');
	app.use(passport.initialize());
	app.use(passport.session());
        // initiate connectors
        config.connectors
            .filter(obj => obj.active)
            .map(obj => {
                var connector = require('../connectors/'+obj.provider);
                var router = connector.route(
                    express.Router(),
                    passport,
                    middleware['isAuthenticated']
                );
                connector.init(passport);
		app.use("/"+obj.provider, router);
            });
        // setup authentication and link to database
        passport.serializeUser(function(user, cb) {
            cb(null, user.id);
        });
        passport.deserializeUser(function(id, cb){
	    User.find.byId(id)
	        .then(user => {
                    cb(null, user)
                })
	        .catch(err => cb(err));
        });
    },
    authorization: function(app){
//	app.use('/oauth', require('../routes/oauth2'))
    },
    port: config.server.port
};


module.exports.render = {
    asHTML: function(view_name){
	var def = q.defer();
	ejs.renderFile('views/'+view_name+'.ejs', function(err, str){
	    if(err) def.reject(err)
	    else def.resolve(str)
	});
	return def.promise;
    }
}

module.exports.mailer = {
    send: function(subject, recipient, from, html_tmpl, text_tmpl){
	var def = q.defer(),
            mailOptions = {
                from: config.mail.lost_password.from,
                to: recipient,
                subject: config.mail.lost_password.subject,
                text: config.mail.lost_password.text,
                html: config.mail.lost_password.html
            };

	setTimeout(function(){
	    def.resolve('ok')
	}, 1000)
        // email.sendMail(mailOptions, function(err, info){
	//     console.log(info)
	//     if(err) def.reject(err)
	//     else def.resolve(info)
        // });
	return def.promise;
    }
}

module.exports.crypto = {
    _algorithm: 'aes-256-ctr',
    encode: function(data){
	var cipher = crypto.createCipher(this._algorithm, config.server.secret_key)
        var crypted = cipher.update(JSON.stringify(data),'utf8','hex')
        crypted += cipher.final('hex');
	console.log(crypted)
        return q.when(crypted);
    },
    decode: function(encrypted_text){
	var decipher = crypto.createDecipher(this._algorithm, config.server.secret_key)
        var dec = decipher.update(encrypted_text,'hex','utf8')
        dec += decipher.final('utf8');
	try{
	    dec = JSON.parse(dec);
	}catch(err){
	    dec = null;
	}
        return q.when(dec);
    }
}
