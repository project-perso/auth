var Pool = require('pg').Pool,
    config = require('../../config.json')['database'];


var pool = new Pool(config);

pool.on('error', function(e, client) {
    console.error("Error"+new Date.toISOString()+"\t"+JSON.stringify(e))
});

module.exports = pool;
