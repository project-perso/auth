var User = require('../models/user'),
    bcrypt = require('bcrypt-nodejs');

module.exports.local = {
    login: function (req, email, password, cb){
	if (email) email = email.toLowerCase();
	User.find(email, 'local')
	    .then(e => User.verifyPassword(email, password))
	    .then(user => cb(null, user))
	    .catch(err => cb(err))
    },
    register: function (req, email, password, cb) {
	// TODO hash password
	if (email) email = email.toLowerCase();
	_manageUser({
	    id: profile.id,
	    data: {
		email: email,
		password: password
	    }
	}, 'local', req.user).then(user => cb(null, user)).catch(err => cb(err))
    }
}




module.exports.google = function(req, access_token, refreshToken, profile, cb) {
    _manageUser({
	id: profile.id,
	data: {
	    profile: profile,
	    token: {
		access_token: access_token,
		refresh_token: refresh_token
	    }
	}
    }, 'google', req.user).then(user => cb(null, user)).catch(err => cb(err))
};

module.exports.facebook = function(req, access_token, refresh_token, profile, cb){
    _manageUser({
	id: profile.id,
	data: {
	    profile: profile,
	    token: {
		access_token: access_token,
		refresh_token: refresh_token
	    }
	}
    }, 'facebook', req.user).then(user => cb(null, user)).catch(err => cb(err))
};


module.exports.twitter = function(req, access_token, refrsh_token, profile, cb){
}


function _manageUser(user, provider, session_user){
    // user is logged in => update current record
    if(session_user){
	return User.find(session_user.id, {provider: provider, keys:'id'})
	    .then(user => User.update(session_user.id, user.data, provider))
    }
    // new user we don't know about
    else{
	return User.find([user.id], {provider: provider, keys: 'id:email'})
	    .then(user => q.reject('already token'))
	    .catch(err=> {
		User.insert(user.data, method)
	    });
    }
}
