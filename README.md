# Introduction
A Framework agnostic user authentication system to CRUD users and allow them to register using different connectors.
Currently, available connectors are:
- local
- facebook
- google

# Demo
That's what we're using at [youplo](http://youplo.com)

# Getting started
## Install
```
git clone https://gitlab.com/project-perso/auth
npm install
```

## Config
the config.json is where all the configuration happen. You'll need to setup
```

```
## Database

The only supported database is Postgres 9.5
```
CREATE DATABASE auth;
\c auth

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE auth_user (
    id uuid UNIQUE DEFAULT uuid_generate_v1(),
    creation_date timestamp DEFAULT NOW(),
    status VARCHAR(16),
    PRIMARY KEY (id)
);

CREATE TABLE auth_user_account (
    related_user uuid REFERENCES auth_user(id) ON DELETE cascade,
    provider VARCHAR(16) NOT NULL,
    profile jsonb NOT NULL
);

CREATE INDEX user_active_idx ON auth_user(status);
CREATE INDEX provider ON auth_user_account(provider);
CREATE INDEX user_emails_gin_idx ON auth_user_account USING gin (profile jsonb_path_ops);
```
## Run
```
npm run start
```