 var express = require('express'),
     bootstrap = require('./src/lib/helper')['init'],
     app = express();

bootstrap.setup(app);
bootstrap.authentication(app);
//bootstrap.authorization(app);

app.use('/assets/', express.static("views/assets"))
app.use('/', require('./src/routes/home'))

app.listen(bootstrap.port, function(err){
    if(err) console.error(err)
    else console.log('The magic happens on port ' + bootstrap.port);
});
